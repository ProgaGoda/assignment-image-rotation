#include "transform.h"

struct image rotate_left(struct image const src) {
  struct dimensions dims = dimensions_reverse(src.dims);
  struct image img = image_create(dims);
  for (uint32_t x = 0; x < dims.width; x = x + 1) {
    for (uint32_t y = 0; y < dims.height; y = y + 1) {
      img.data[x + y * dims.width] =
          src.data[(dims.width - x - 1) * dims.height + y];
    }
  }
  return img;
}
